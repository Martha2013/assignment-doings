package com.martha.assignmentone;



import android.os.Bundle;
import android.app.Activity;

import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends Activity {
	Button btncomplete;
	String phone, pwd, name, year, gender, course;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Intent strings= getIntent();
		phone=strings.getStringExtra("PHONE");
		TextView txt=(TextView)findViewById(R.id.lblphone);
		txt.setText(phone);
		
		pwd=strings.getStringExtra("PASSWORD");
		TextView txt1=(TextView)findViewById(R.id.lblpwd);
		txt1.setText(pwd);
		
		name=strings.getStringExtra("NAME");
		TextView txt2=(TextView)findViewById(R.id.lblName);
		txt2.setText(name);
		
		Toast.makeText(getApplicationContext(), "Thank you" + "  " + name + "   " + "for registering", Toast.LENGTH_LONG).show();
		
		year=strings.getStringExtra("YEAR");
		TextView txt3=(TextView)findViewById(R.id.lblyear);
		txt3.setText(year);
		
		gender=strings.getStringExtra("GENDER");
		TextView txt4=(TextView)findViewById(R.id.lblgender);
		txt4.setText(gender);
		
		course=strings.getStringExtra("COURSE");
		TextView txt5=(TextView)findViewById(R.id.lblcourse);
		txt5.setText(course);
		
		btncomplete=(Button)findViewById(R.id.btnreg);
		btncomplete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent regIntent=new Intent(getApplicationContext(),Registration.class);
				startActivity(regIntent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.screen2, menu);
		return true;
	}

}
