package com.martha.assignmentone;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;



import android.widget.RadioGroup;
import android.widget.Spinner;


public class Registration extends Activity implements OnItemSelectedListener {
	Button SAVE;
	//RadioButton Male, Female;
	RadioGroup GENDERS;
	
	EditText txtName;
	 Spinner Spinner1, Spinner2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		
		
		txtName=(EditText)findViewById(R.id.txtNAME);
		Spinner1=(Spinner)findViewById(R.id.spinner1);
		Spinner2=(Spinner)findViewById(R.id.spinner2);
        Spinner1.setOnItemSelectedListener(this); 
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.birth_array, android.R.layout.simple_spinner_item); //default spinner appearance
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //standard layout defined by platform
        Spinner1.setAdapter(adapter);
        
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.course_array, android.R.layout.simple_spinner_item); 
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner2.setAdapter(adapter1);
		
        GENDERS=(RadioGroup)findViewById(R.id.rgpGENDERS);
       
	SAVE=(Button)findViewById(R.id.btnback);
	SAVE.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View parent) {
			// TODO Auto-generated method stub
			
			 int numgender = GENDERS.getCheckedRadioButtonId();
			 
			RadioButton GENDERS =(RadioButton)findViewById(numgender);
			String gender=GENDERS.getText().toString();
			
			
			 
			
			Intent saveIntent=new Intent(getApplicationContext(),Home.class);
			saveIntent.putExtra("NAME", txtName.getText().toString());
			
			saveIntent.putExtra("YEAR", Spinner1.getSelectedItem().toString());
			saveIntent.putExtra("COURSE", Spinner2.getSelectedItem().toString());
			saveIntent.putExtra("GENDER",gender);
			
			startActivity(saveIntent);
			
		}
	});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.screen_three, menu);
		return true;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}

}
