package com.martha.assignmentone;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends Activity {
	Button btnlogin;
    EditText txtNumber, txtdots;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtNumber=(EditText)findViewById(R.id.txtphone);
        txtdots=(EditText)findViewById(R.id.txtpwd);
        btnlogin=(Button)findViewById(R.id.btnlogin);
        btnlogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent loginIntent =new Intent(getApplicationContext(),Home.class); //create a new next object
				loginIntent.putExtra("PHONE", txtNumber.getText().toString());
				loginIntent.putExtra("PASSWORD", txtdots.getText().toString());
				startActivity(loginIntent);
				
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sign_up, menu);
        return true;
    }
    
}
